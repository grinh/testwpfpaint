﻿using System.Diagnostics;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace TestWpfPaint
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private bool draw = false;
        private Point lastPoint;

        public MainWindow()
        {
            InitializeComponent();
            MyCanvas.MouseMove += MyCanvasOnMouseMove;
            MyCanvas.MouseDown += MyCanvasMouseDown;
            MyCanvas.MouseUp += MyCanvasMouseUp;
            MyCanvas.MouseLeave += MyCanvasOnMouseLeave;
            ClearButton.Click += ClearButtonOnClick;
        }

        private void ClearButtonOnClick(object sender, RoutedEventArgs routedEventArgs)
        {
            MyCanvas.Children.Clear();
        }

        private void MyCanvasOnMouseLeave(object sender, MouseEventArgs mouseEventArgs)
        {
            draw = false;
        }

        private void MyCanvasMouseUp(object sender, MouseButtonEventArgs e)
        {
            draw = false;
        }

        private void MyCanvasMouseDown(object sender, MouseButtonEventArgs e)
        {
            draw = true;
            lastPoint = e.GetPosition(this);
        }

        private void MyCanvasOnMouseMove(object sender, MouseEventArgs e)
        {
            if (draw)
            {
                Line line = new Line();

                line.Stroke = new SolidColorBrush(Colors.Red);
                line.X1 = lastPoint.X;
                line.Y1 = lastPoint.Y;
                line.X2 = e.GetPosition(this).X;
                line.Y2 = e.GetPosition(this).Y;

                lastPoint = e.GetPosition(this);

                MyCanvas.Children.Add(line);

                Debug.WriteLine("line added");
            }
        }
    }
}
